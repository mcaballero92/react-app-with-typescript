import useNewSubForm from '../hooks/useNewSubForm'
import { Sub } from '../types'

interface FormProps {
    onNewSub: (newSub: Sub) => void
}

const Form = ({ onNewSub }: FormProps) => {
    /*const [inputValues, setInputValues] = useState<FormState["inputValues"]>(
        INITIAL_STATE
    )*/

    const [inputValues, distpatch] = useNewSubForm()

    const handleClear = () => {
    
        distpatch(
            {
                type: 'clear',
            }
        )

    }

    const handleSubmit = (evt: React.FormEvent<HTMLFormElement>) => {
        evt.preventDefault();
        onNewSub(inputValues)
        distpatch(
            {
                type: 'clear',
            }
        )
    }

    const handleChange = (evt: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const {name, value} = evt.target
        distpatch(
            {
                type: 'change_value',
                payload:{
                    inputName: name,
                    inputValue: value
                }
            }
        )
     

    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <input onChange={handleChange} type='text' value={inputValues.nick} name='nick' placeholder="nick"></input>
                <input onChange={handleChange} type='number' value={inputValues.subMonths} name='subMonths' placeholder="subMonths"></input>
                <input onChange={handleChange} type='text' value={inputValues.avatar} name='avatar' placeholder="avatar"></input>
                <textarea onChange={handleChange} value={inputValues.description} name='description' placeholder="description"></textarea>
                <button onClick={handleClear} type="button">Clear the form</button>
                <button type="submit">Save new sub!</button>
            </form>
        </div>
    )

}

export default Form